package com.bankaks.billpay.model.api_client

import com.bankaks.billpay.model.*
import io.reactivex.Observable

class ApiClient : ApiMethod {

    companion object {
        const val BASE_URL: String = "https://api-staging.bankaks.com/"
    }

    constructor()

    fun postGetData(selectedIndex: Int): Observable<DataResponse> {
        return RxGetRequest(
            BASE_URL.plus("task/").plus(selectedIndex),
            HashMap()
        ).getObjectObservable(DataResponse::class.java)
    }
}
