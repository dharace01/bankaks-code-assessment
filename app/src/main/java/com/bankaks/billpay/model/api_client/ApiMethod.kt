package com.bankaks.billpay.model.api_client

import com.androidnetworking.common.Priority
import com.rx2androidnetworking.Rx2ANRequest
import com.rx2androidnetworking.Rx2AndroidNetworking
import org.json.JSONObject
import java.util.*

open class ApiMethod {

    fun RxGetRequest(path: String, body: HashMap<String, String>): Rx2ANRequest {
        return Rx2AndroidNetworking.get(path) //                .addHeaders(headers)
                .addQueryParameter(body)
                .setTag("GetRequest")
                .setPriority(Priority.HIGH)
                .build()
    }

}