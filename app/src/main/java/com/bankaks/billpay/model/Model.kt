package com.bankaks.billpay.model

import android.os.Parcelable
import androidx.annotation.Keep
import kotlinx.android.parcel.Parcelize
import java.io.Serializable

enum class ResponseCode constructor(val code: Int) {
    OK(200),
    BadRequest(400),
    Unauthenticated(401),
    Unauthorized(403),
    NotFound(404),
    RequestTimeOut(408),
    Conflict(409),
    InValidateData(422),
    Blocked(423),
    ForceUpdate(426),
    ServerError(500);
}

@Parcelize
data class DataResponse(
    val status: Boolean? = false,
    val result: Result? = null,
    val message: String? = ""
) : Parcelable

@Parcelize
data class Result(
    val number_of_fields: Int = 0,
    val screen_title: String? = "",
    val operator_name: String? = "",
    val service_id: String? = "",
    val fields: ArrayList<Fields>? = null
) : Parcelable

@Parcelize
data class Fields(
    val name: String? = "",
    val placeholder: String? = "",
    val regex: String? = "",
    val is_mandatory: Boolean? = false,
    val hint_text: String? = "",
    val type: Type? = null,
    val ui_type: UIType? = null,
    var viewId: Int = -1
) : Parcelable

@Parcelize
data class Type(
    val data_type: String? = "",
    val array_name: String? = "",
    val is_array: Boolean? = false,
) : Parcelable

@Parcelize
data class UIType(
    val type: String? = "",
    val values: ArrayList<Values>? = null
) : Parcelable

@Parcelize
data class Values(
    val name: String? = "",
    val id: String? = "",
) : Parcelable




