package com.bankaks.billpay.base

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import com.androidnetworking.error.ANError
import com.bankaks.billpay.R
import com.bankaks.billpay.model.ResponseCode
import com.bankaks.billpay.model.api_client.ApiClient
import com.bankaks.billpay.pref.AppPref
import com.bankaks.billpay.ui.SplashScreen
import com.bankaks.billpay.util.CheckInternetConnection
import com.bankaks.billpay.util.HideDialog
import com.bankaks.billpay.util.ShowDialog
import com.bumptech.glide.load.HttpException
import com.google.gson.Gson
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import org.jetbrains.anko.*
import org.json.JSONObject
import java.net.ConnectException
import java.net.SocketTimeoutException

open class BaseActivity(@LayoutRes private val contentLayoutId: Int) :
    AppCompatActivity() {

    protected val apiClient = ApiClient()

    var disposable: Disposable? = null

//    private var deleteAddressDialog: DeleteAddressDialog? = null
//    private var deleteFavoriteDialog: DeleteFavoriteDialog? = null
//    private var saveInFavDialog: SaveInFavDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(contentLayoutId)
    }

    fun <T> callApi(
        observable: Observable<T>,
        showLoader: Boolean = true,
        responseBlock: (T) -> Unit
    ) {
        if (!CheckInternetConnection()) {
            return
        }

        observable.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<T> {
                override fun onSubscribe(d: Disposable) {
                    if (showLoader) {
                        ShowDialog()
                    }
                }

                override fun onNext(response: T) {
                    Log.e("API", "Success : ${Gson().toJson(response)}")
                    responseBlock(response)
                }

                override fun onError(e: Throwable) {
                    if (showLoader) {
                        HideDialog()
                    }

                    onResponseFailure(e)
                }

                override fun onComplete() {
                    if (showLoader) {
                        HideDialog()
                    }
                }
            })

        /*
        observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (hideOnSuccess) {
                        onResponse()
                    }
                    responseBlock(it)
                }, {
                    onResponseFailure(it, hideOnFail)
                })
        */
    }

    private fun onResponseFailure(throwable: Throwable) {
        var error = throwable as (ANError)
        Log.e("API", "Error : ${error.message.toString()}")
        Log.e("API", "Error : ${error.errorCode}")
        when (throwable) {
            is HttpException -> {
                handleResponseError(throwable)
            }
            is ConnectException -> {
                sneakerError(getString(R.string.msg_no_internet))
            }
            is SocketTimeoutException -> {
                sneakerError(getString(R.string.time_out))
            }
        }
    }

    private fun handleResponseError(throwable: ANError) {
        when (throwable.errorCode) {
            ResponseCode.InValidateData.code -> {
                val errorRawData = throwable.errorBody
                if (!errorRawData.isNullOrEmpty()) {
                    val jsonObject = JSONObject(errorRawData)
                    val jObject = jsonObject.optJSONObject("errors")
                    if (jObject != null) {
                        val keys: Iterator<String> = jObject.keys()
                        if (keys.hasNext()) {
                            val msg = StringBuilder()
                            while (keys.hasNext()) {
                                val key: String = keys.next()
                                if (jObject.get(key) is String) {
                                    msg.append("- ${jObject.get(key)}\n")
                                }
                            }
                            errorDialog(msg.toString(), getString(R.string.alert))
                        } else {
                            errorDialog(jsonObject.optString("message", ""))
                        }
                    } else {
                        errorDialog(
                            JSONObject(errorRawData).optString("message"),
                            getString(R.string.alert)
                        )
                    }
                }
            }
            ResponseCode.Unauthenticated.code -> {
                val errorRawData = throwable.errorBody
                if (!errorRawData.isNullOrEmpty()) {
                    alert(errorRawData, getString(R.string.alert)) {
                        okButton { onAuthFail() }
                    }.show()
                } else {
                    onAuthFail()
                }
            }
            ResponseCode.ForceUpdate.code -> {

            }
            ResponseCode.ServerError.code -> errorDialog(getString(R.string.internal_server_error))
            ResponseCode.BadRequest.code,
            ResponseCode.Unauthorized.code,
            ResponseCode.NotFound.code,
            ResponseCode.RequestTimeOut.code,
            ResponseCode.Conflict.code,
            ResponseCode.Blocked.code -> {
                val errorRawData = throwable.errorBody
                if (!errorRawData.isNullOrEmpty()) {
                    errorDialog(JSONObject(errorRawData).optString("message", ""))
                }
            }
        }
    }

    private fun errorDialog(optString: String?, title: String = getString(R.string.app_name)) {
        optString?.let {
//            sneakerError(it)
            alert(it, title) { okButton { } }.build().show()
        }
    }

    fun sneakerError(message: String) {
        toast(message)
    }

    private fun onAuthFail() {
        AppPref.clearPref()
        startActivity(intentFor<SplashScreen>().clearTask().newTask())
    }

    fun hideKeyboard() {
        var view = currentFocus
        if (view == null) {
            view = View(this)
        }
        val inputMethodManager =
            getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }

    /*
    fun deleteAddressDialog(
            activity: Activity,
            addressName: String,
            onClickOk: BaseFragment.OnClickDialog
    ) {
        if (!activity.isFinishing) {
            deleteAddressDialog = DeleteAddressDialog(
                    activity, addressName,
                    onClickOk
            )
            deleteAddressDialog!!.run()
        }
    }

    fun deleteFromFavDialog(
            activity: Activity,
            shopName: String,
            onClickOk: BaseFragment.OnClickDialog
    ) {
        if (!activity.isFinishing) {
            deleteFavoriteDialog = DeleteFavoriteDialog(
                    activity, shopName,
                    onClickOk
            )
            deleteFavoriteDialog!!.run()
        }
    }

    fun addToFavDialog(
            activity: Activity,
            shopName: String,
            onClickOk: BaseFragment.OnClickDialog
    ) {
        if (!activity.isFinishing) {
            saveInFavDialog = SaveInFavDialog(
                    activity, shopName,
                    onClickOk
            )
            saveInFavDialog!!.run()
        }
    }
    */
}