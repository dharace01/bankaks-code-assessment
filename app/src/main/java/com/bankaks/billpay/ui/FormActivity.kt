package com.bankaks.billpay.ui

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.InputType
import android.util.Log
import android.util.Patterns
import android.view.Gravity
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.*
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bankaks.billpay.R
import com.bankaks.billpay.base.BaseActivity
import com.bankaks.billpay.base.BaseAdapter
import com.bankaks.billpay.model.DataResponse
import com.bankaks.billpay.model.Fields
import com.bankaks.billpay.model.Values
import com.google.android.material.textfield.TextInputEditText
import kotlinx.android.synthetic.main.activity_form.*
import kotlinx.android.synthetic.main.item_form.view.*
import java.util.regex.Pattern

class FormActivity : BaseActivity(R.layout.activity_form), View.OnClickListener {

    var data: DataResponse? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
        initOnClick()
    }

    private fun initView() {
        data = intent.getParcelableExtra<DataResponse>("data")
        tvTitle.text = data?.result?.screen_title

        setListAdapter(data)
    }

    private fun setListAdapter(data: DataResponse?) {
        // set form list
        val fields: ArrayList<Fields> = data?.result?.fields!!
        formList.layoutManager = LinearLayoutManager(this)
        val fieldsAdapter = object : BaseAdapter<Fields>(this, fields) {

            override fun onBindData(holder: RecyclerView.ViewHolder, item: Fields, position: Int) {

                when (item.ui_type?.type) {
                    "textfield" -> {

                        holder.itemView.llTextField.visibility = View.VISIBLE
                        holder.itemView.llDropdown.visibility = View.GONE
                        holder.itemView.llTextField.isVisible
                        holder.itemView.llDropdown.isVisible

                        holder.itemView.textInput.hint = item.placeholder
                        holder.itemView.textInput.placeholderText = item.hint_text

                        item.viewId = holder.itemView.editText.id
                        Log.e("id", item.viewId.toString())

                        if (item.type!!.data_type == "int") {
                            holder.itemView.editText.inputType = InputType.TYPE_CLASS_NUMBER
                        }
                    }
                    "dropdown" -> {

                        holder.itemView.llDropdown.visibility = View.VISIBLE
                        holder.itemView.llTextField.visibility = View.GONE
                        !holder.itemView.llTextField.isVisible
                        holder.itemView.llDropdown.isVisible

                        holder.itemView.tvHint.text = item.placeholder
                        item.ui_type.values?.let {
                            setDropDown(
                                holder.itemView.spOptions,
                                it,
                                item.hint_text,
                                item
                            )
                        }
                    }
                    else -> {
                    }
                }
            }

            override fun setItemLayout(): Int {
                return R.layout.item_form
            }

            override fun setNoDataView(): TextView? {
                return null
            }
        }
        formList.adapter = fieldsAdapter
    }

    private fun setDropDown(
        spOptions: Spinner,
        values: ArrayList<Values>,
        hintText: String?,
        item: Fields
    ) {

        var list: ArrayList<String> = ArrayList()
        for (value in values) {
            list.add(value.name.toString())
        }

        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, list)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spOptions.adapter = adapter

        // default selection at first position
        spOptions.setSelection(0, true);
        val v: TextView = spOptions.selectedView as TextView
        v.setTextColor(resources.getColor(R.color.colorAccent))
        v.text = hintText
        item.viewId = -1;

        spOptions.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parentView: AdapterView<*>?,
                selectedItemView: View?,
                pos: Int,
                id: Long
            ) {
                val v: TextView = spOptions.selectedView as TextView
                v.setTextColor(resources.getColor(R.color.colorAccent))
                item.viewId = pos
            }

            override fun onNothingSelected(parentView: AdapterView<*>?) {
                // your code here
            }
        }
    }

    private fun initOnClick() {
        ivBack.setOnClickListener(this)
        btnProceed.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.ivBack -> {
                onBackPressed()
            }
            R.id.btnProceed -> {
                checkValidate()
            }
        }
    }

    var i = 0
    private fun checkValidate() {
        if (i < data!!.result!!.number_of_fields) {
            val field: Fields = data!!.result!!.fields!![i]
            if (field.is_mandatory!!) {
                if (field.ui_type!!.type == "textfield") {
                    val row: LinearLayout =
                        formList.getLayoutManager()!!.findViewByPosition(i) as LinearLayout
                    val editText = row.findViewById<TextInputEditText>(R.id.editText)
                    val dataString = editText.text.toString()
                    if (dataString.isEmpty()) {
                        showDialogError("Please enter " + field.placeholder + " !", false)
                    } else if (field.regex!!.isNotEmpty()) {
                        var pattern : Pattern? = null
                        if (field.placeholder!!.contains("email")) {
                            pattern = Patterns.EMAIL_ADDRESS
                        } else {
                            pattern = Pattern.compile(field.regex)
                        }
                        var matcher = pattern.matcher(dataString)
                        if (matcher.find()) {
                            i++
                            checkValidate()
                        } else {
                            showDialogError("Please enter valid " + field.placeholder + " !", false)
                        }
                    } else {
                        i++
                        checkValidate()
                    }
                } else if (field.ui_type.type == "dropdown") {
                    if (field.viewId == -1) {
                        showDialogError("Please enter valid " + field.placeholder + " !", false)
                    } else {
                        i++
                        checkValidate()
                    }
                } else {
                    i++
                    checkValidate()
                }
            }
        } else {
            var string = ""
            for ((index, field) in data!!.result!!.fields!!.withIndex()) {
                val row: LinearLayout =
                    formList.getLayoutManager()!!.findViewByPosition(index) as LinearLayout
                if (field.ui_type!!.type == "textfield") {
                    val editText = row.findViewById<TextInputEditText>(R.id.editText)
                    val dataString = editText.text.toString()
                    string = string.plus(field.placeholder.plus(" : ") + dataString + "\n")
                } else {
                    string =
                        string + field.placeholder.plus(" : ") + field.ui_type.values!![field.viewId].name + "\n"
                }
            }
            showDialogError(string, true)
        }
    }


    private fun showDialogError(errorMessage: String, isSuccess: Boolean) {

        val dialog = Dialog(this@FormActivity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        dialog.setContentView(R.layout.dlg_error)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window!!.setDimAmount(0.8f)

        val window = dialog.window
        val wlp = window!!.attributes
        wlp.width = WindowManager.LayoutParams.MATCH_PARENT
        wlp.gravity = Gravity.CENTER
        dialog.show()
        dialog.window!!.attributes = wlp

        val txtError = dialog.findViewById(R.id.txtError) as TextView
        txtError.text = errorMessage

        val tvYes = dialog.findViewById(R.id.tvYes) as TextView
        tvYes.setOnClickListener {
            dialog.dismiss()
            if (isSuccess) finish()
        }
    }
}