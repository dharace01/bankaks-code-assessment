package com.bankaks.billpay.ui

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import com.bankaks.billpay.R
import com.bankaks.billpay.base.BaseActivity
import org.jetbrains.anko.clearTask
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.newTask

class SplashScreen : BaseActivity(R.layout.activity_splash_screen) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
    }

    private fun initView() {
        Handler(Looper.getMainLooper()).postDelayed({
            startActivity(intentFor<SelectionActivity>().clearTask().newTask())
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        }, 3000)
    }
}