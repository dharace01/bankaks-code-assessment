package com.bankaks.billpay.ui

import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import com.bankaks.billpay.R
import com.bankaks.billpay.base.BaseActivity
import kotlinx.android.synthetic.main.activity_selection.*
import org.jetbrains.anko.clearTask
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.newTask

class SelectionActivity : BaseActivity(R.layout.activity_selection), View.OnClickListener {

    var position: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
        initOnClick()
    }

    private fun initView() {
        setDropDown()
    }

    private fun initOnClick() {
        btnProceed.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.btnProceed -> {
                callProceedApi(position + 1)
            }
        }
    }

    // set dropdown to select options
    private fun setDropDown() {
        val paths = arrayOf("Option 1", "Option 2", "Option 3")
        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, paths)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spOptions.adapter = adapter

        // default selection at first position
        spOptions.setSelection(position, true);
        val v: View = spOptions.selectedView
        (v as TextView).setTextColor(resources.getColor(R.color.colorAccent))

        spOptions.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parentView: AdapterView<*>?,
                selectedItemView: View?,
                pos: Int,
                id: Long
            ) {
                (selectedItemView as TextView).setTextColor(resources.getColor(R.color.colorAccent))
                position = pos
            }

            override fun onNothingSelected(parentView: AdapterView<*>?) {
                // your code here
            }
        }
    }

    // call api to get form details
    private fun callProceedApi(selectedIndex: Int) {
        callApi(apiClient.postGetData(selectedIndex), true) {
            if (it.status!!) {
                startActivity(intentFor<FormActivity>().putExtra("data", it))
            } else {
                it.message?.let { it1 -> sneakerError(it1) }
            }
        }
    }
}